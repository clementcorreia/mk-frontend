import { Creative, User } from "../utils/interfaces";
import {
  Button,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
} from "@mui/material";
import { Person } from "@mui/icons-material";
import moment from "moment";
import "moment/locale/fr";

interface CreativeDynamicPreviewProps {
  creative?: Creative;
  handleClickEdit: () => void;
}

function CreativeDynamicPreview(props: CreativeDynamicPreviewProps) {
  if (!props.creative) {
    return null;
  }

  moment.locale("fr");

  return (
    <Paper style={{ padding: 16 }} elevation={8}>
      <Grid container spacing={3}>
        <Grid item xs={8}>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              height: "100%",
            }}
          >
            <div style={{ flex: 1 }}>
              <Typography variant="h6" paragraph role={"dynamicPreviewTitle"}>
                {props.creative?.title}
              </Typography>
              <Typography paragraph role="dynamicPreviewDescription">
                {props.creative?.description}
              </Typography>
              <Typography paragraph role="dynamicPreviewContent">
                {props.creative?.content}
              </Typography>
            </div>
            <div>
              <Button
                onClick={() => props.handleClickEdit()}
                role="dynamicPreviewEditButton"
              >
                Modifier
              </Button>
            </div>
          </div>
        </Grid>
        <Grid item xs={4}>
          <Paper elevation={0} style={{ padding: 16 }}>
            <Typography
              paragraph
              variant="subtitle2"
              role="dynamicPreviewCreatedBy"
            >
              {`Créé par ${props.creative?.createdBy?.firstName} ${props.creative?.createdBy?.lastName}`}
            </Typography>
            <Typography
              paragraph
              variant="subtitle2"
              role="dynamicPreviewLastModifiedDate"
            >
              {`Dernière modification le ${moment(
                props.creative?.lastModified
              ).format("LL")}`}
            </Typography>
          </Paper>

          <Paper elevation={2}>
            <List>
              {props.creative?.contributors?.length ? (
                props.creative?.contributors?.map((contributor: User) => (
                  <PersonItem
                    role="dynamicPreviewContributors"
                    key={contributor.id}
                    contributor={contributor}
                  />
                ))
              ) : (
                <Typography variant="subtitle2" sx={{ m: 1 }}>
                  Aucun contributeur
                </Typography>
              )}
            </List>
          </Paper>
        </Grid>
      </Grid>
    </Paper>
  );
}

interface PersonItemProps {
  contributor: User;
  role?: string;
}

function PersonItem(props: PersonItemProps) {
  return (
    <ListItem role={props.role}>
      <ListItemIcon>
        <Person />
      </ListItemIcon>
      <ListItemText
        primary={`${props.contributor.firstName} ${props.contributor.lastName}`}
      />
    </ListItem>
  );
}

export default CreativeDynamicPreview;
