import { render, screen } from "@testing-library/react";
import { testCreative } from "../utils/testableFixture";
import CreativeDynamicPreview from "./CreativeDynamicPreview";

test("CreativeDynamicPreview should render properly", () => {
  render(
    <CreativeDynamicPreview
      creative={testCreative}
      handleClickEdit={() => {}}
    />
  );
  const title = screen.getByRole("dynamicPreviewTitle");
  expect(title.innerHTML).toEqual("Titre 1");
  const content = screen.getByRole("dynamicPreviewContent");
  expect(content.innerHTML).toEqual("My creative content");
  const description = screen.getByRole("dynamicPreviewDescription");
  expect(description.innerHTML).toEqual("My creative description");
  const createdBy = screen.getByRole("dynamicPreviewCreatedBy");
  expect(createdBy.innerHTML).toEqual("Créé par Clément Correia");
  const contributors = screen.getAllByRole("dynamicPreviewContributors");
  expect(contributors.length).toEqual(2);
  const lastModifiedDate = screen.getByRole("dynamicPreviewLastModifiedDate");
  expect(lastModifiedDate.innerHTML).toEqual(
    "Dernière modification le 1 janvier 2022"
  );
  const editButton = screen.getByRole("dynamicPreviewEditButton");
  expect(editButton.innerHTML).toContain("Modifier");
});
