import { List, Paper } from "@mui/material";
import { Creative } from "../utils/interfaces";
import CreativeListItem from "./CreativeListItem";

interface CreativeListProps {
  creatives: Creative[];
  onClickItem?: (id: string) => void;
  selectedId?: string;
}

function CreativeList(props: CreativeListProps) {
  return (
    <Paper style={{ padding: 16 }} elevation={8}>
      <List>
        {props.creatives?.map((creative, index) => (
          <CreativeListItem
            key={creative.id}
            creative={creative}
            divider={index < props.creatives?.length - 1}
            onClick={() => {
              if (props.onClickItem) {
                props.onClickItem(creative.id);
              }
            }}
            selected={props.selectedId === creative.id}
          />
        ))}
      </List>
    </Paper>
  );
}

export default CreativeList;
