import { render, screen } from "@testing-library/react";
import { testCreative } from "../utils/testableFixture";
import CreativeListItem from "./CreativeListItem";

test("CreativeListItem should render properly", () => {
  render(<CreativeListItem creative={testCreative} />);
  const title = screen.getByRole("listItemTitle");
  expect(title.innerHTML).toEqual("Titre 1");
  const contributors = screen.getAllByRole("listItemContributors");
  expect(contributors.length).toEqual(2);
  const formats = screen.getAllByRole("listItemFormats");
  expect(formats.length).toEqual(3);
  const enabled = screen.getByRole("listItemEnabled");
  expect(enabled).toBeChecked();
});
