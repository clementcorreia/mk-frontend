import {
  Button,
  Chip,
  Grid,
  IconButton,
  Paper,
  Switch,
  TextField,
} from "@mui/material";
import { Add } from "@mui/icons-material";
import { Creative, Format } from "../utils/interfaces";
import { formatToString } from "../utils/functions";
import React from "react";

interface CreativeFormProps {
  addingFormatMode?: boolean;
  creative: Creative;
  handleAddFormat: () => void;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleValidateFormat: (format: Format) => void;
}

function CreativeForm(props: CreativeFormProps) {
  const [format, setFormat] = React.useState<Format>({} as Format);
  return (
    <Paper elevation={8} style={{ padding: 16 }}>
      <Grid container alignItems="center">
        <Grid item xs={8}>
          <TextField
            margin="normal"
            label="Titre"
            name="title"
            value={props.creative.title ?? ""}
            onChange={props.handleChange}
          />
        </Grid>
        <Grid item xs container justifyContent="flex-end">
          <Grid item>
            <Switch
              checked={props.creative.enabled ?? false}
              name="enabled"
              onChange={props.handleChange}
            />
          </Grid>
        </Grid>
      </Grid>

      <TextField
        margin="normal"
        fullWidth
        multiline
        minRows={3}
        label="Description"
        name="description"
        value={props.creative.description ?? ""}
        onChange={props.handleChange}
      />

      <TextField
        margin="normal"
        fullWidth
        multiline
        minRows={10}
        label="Contenu"
        name="content"
        value={props.creative.content ?? ""}
        onChange={props.handleChange}
      />

      <Grid container spacing={2} alignItems="center">
        {props.creative?.formats?.map((format: Format) => (
          <Grid item key={formatToString(format)}>
            <Chip label={formatToString(format)} color="primary" />
          </Grid>
        ))}
        <Grid item xs={props.addingFormatMode ? 12 : undefined}>
          {props.addingFormatMode ? (
            <Grid container spacing={2} alignItems="center">
              <Grid item>
                <TextField
                  label="Largeur"
                  name="width"
                  type="number"
                  margin="dense"
                  size="small"
                  value={format?.width ?? ""}
                  onChange={(event) =>
                    setFormat((prev) => ({
                      ...prev,
                      width: parseInt(event.target.value),
                    }))
                  }
                />
              </Grid>
              <Grid item>x</Grid>
              <Grid item>
                <TextField
                  label="Hauteur"
                  name="height"
                  type="number"
                  margin="dense"
                  size="small"
                  value={format?.height ?? ""}
                  onChange={(event) =>
                    setFormat((prev) => ({
                      ...prev,
                      height: parseInt(event.target.value),
                    }))
                  }
                />
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  onClick={
                    format.width && format.height
                      ? () => {
                          props.handleValidateFormat(format);
                          setFormat({} as Format);
                        }
                      : undefined
                  }
                >
                  Valider
                </Button>
              </Grid>
            </Grid>
          ) : (
            <IconButton
              onClick={props.handleAddFormat}
              size="small"
              color="primary"
            >
              <Add />
            </IconButton>
          )}
        </Grid>
      </Grid>
    </Paper>
  );
}

export default CreativeForm;
