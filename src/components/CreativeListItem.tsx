import {
  Avatar,
  Chip,
  Grid,
  ListItem,
  ListItemText,
  Switch,
  Typography,
} from "@mui/material";
import { formatToString, getUserInitials } from "../utils/functions";
import { Creative, Format, User } from "../utils/interfaces";

interface CreativeListItemProps {
  creative: Creative;
  divider?: boolean;
  onClick?: (...args: any[]) => any;
  selected?: boolean;
}

function CreativeListItem(props: CreativeListItemProps) {
  return (
    <ListItem
      divider={props.divider}
      onClick={props.onClick}
      secondaryAction={
        <Switch
          checked={props.creative.enabled}
          inputProps={{ role: "listItemEnabled" }}
        />
      }
    >
      <ListItemText
        primary={
          <Grid container spacing={1}>
            <Grid item xs={3}>
              <Typography
                variant="h6"
                style={{
                  ...(props.selected ? { fontWeight: "bold" } : {}),
                  marginRight: 8, // Ajout d'un margin ici pour éviter que le texte passe sous les avatars portant les initiales des contributeurs
                }}
                role="listItemTitle"
              >
                {props.creative.title}
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <div style={{ display: "flex" }}>
                {props.creative.contributors?.length
                  ? props.creative.contributors?.map((user: User) => (
                      <Avatar
                        key={getUserInitials(user)}
                        style={{ marginLeft: -16 }}
                        role="listItemContributors"
                      >
                        {getUserInitials(user)}
                      </Avatar>
                    ))
                  : null}
              </div>
            </Grid>
            <Grid item xs={6}>
              {props.creative.formats?.map((format: Format, index: number) => (
                <Chip
                  style={{ marginRight: 8, marginTop: 4 }} // Ajout d'un margin ici pour éviter que les chips se chevauchent
                  key={`${index}-${formatToString(format)}`}
                  label={`${formatToString(format)}`}
                  role="listItemFormats"
                />
              ))}
            </Grid>
          </Grid>
        }
      />
    </ListItem>
  );
}

export default CreativeListItem;
