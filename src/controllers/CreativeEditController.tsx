import { QueryKey, useMutation, useQuery } from "@tanstack/react-query";
import CreativeForm from "../components/CreativeForm";
import { Creative, Format } from "../utils/interfaces";
import React from "react";
import { Button, Grid } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";

interface CreativeEditControllerProps {
  mode?: "edit" | "create"; // default to "edit"
}

function CreativeEditController(props: CreativeEditControllerProps) {
  let { creativeId } = useParams();
  let navigate = useNavigate();

  const [creative, setCreative] = React.useState<Creative>({} as Creative);
  const [initialCreative, setInitialCreative] = React.useState<Creative>(
    {} as Creative
  );
  const [addingFormatMode, setAddingFormatMode] =
    React.useState<boolean>(false);

  const fetchCreative = ({
    queryKey: [_queryName, creativeId],
  }: {
    queryKey: QueryKey;
  }) =>
    !creativeId
      ? Promise.resolve({})
      : fetch(`http://localhost:3001/creatives/${creativeId}`).then((res) =>
          res.json()
        );

  const { isLoading, isFetching, error, data } = useQuery<Creative>(
    ["getOneCreative", creativeId],
    fetchCreative
  );

  React.useEffect(() => {
    const localCreative = data ?? ({} as Creative);
    setCreative(localCreative);
    setInitialCreative(localCreative); // On garde une copie initiale
    return () => {
      setCreative({} as Creative);
    };
  }, [data]);

  // Si la requête pour récupérer la bannière est terminée on définit
  // la bannière initiale afin de pouvoir annuler les modifications
  React.useEffect(() => {
    if (creative.id && !initialCreative.id) {
    }
    return () => {
      setInitialCreative({} as Creative);
    };
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (["title", "content", "description"].includes(event.target?.name)) {
      setCreative((prev) => ({
        ...(prev ?? {}),
        [event.target.name]: event.target.value,
      }));
    } else if (event.target?.name === "enabled") {
      setCreative((prev) => ({
        ...(prev ?? {}),
        enabled: event.target.checked,
      }));
    }
  };

  const saveCreativeMutation = useMutation<Creative, any, Creative>(
    (creativeUpdates) =>
      fetch(
        `http://localhost:3001/creatives${
          props.mode === "create" ? "" : `/${creativeUpdates.id}`
        }`,
        {
          method: props.mode === "create" ? "POST" : "PUT",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(creativeUpdates),
        }
      ).then((res) => {
        navigate(-1);
        return res.json();
      })
  );

  const deleteCreativeMutation = useMutation<Creative, any, string>(
    (creativeId) =>
      fetch(`http://localhost:3001/creatives/${creativeId}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      }).then((res) => {
        navigate(-1);
        return res.json();
      })
  );

  if (isLoading || isFetching) return <div>Chargement...</div>;

  if (error)
    return <div>Une erreur est survenue : {(error as any).message}</div>;

  if (!data || !creative) return <div>Bannière introuvable</div>;

  return (
    <Grid container spacing={2} justifyContent="center">
      <Grid item xs={12}>
        <CreativeForm
          addingFormatMode={addingFormatMode}
          creative={creative}
          handleAddFormat={() => setAddingFormatMode(true)}
          handleChange={handleChange}
          handleValidateFormat={(newFormat: Format) => {
            setCreative((prev) => ({
              ...prev,
              formats: [...(prev?.formats ?? []), newFormat],
            }));
            setAddingFormatMode(false);
          }}
        />
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={3} justifyContent="center">
          <Grid item>
            <Button
              color="primary"
              variant="contained"
              onClick={() => saveCreativeMutation.mutate(creative)}
            >
              Sauvegarder
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              onClick={() => {
                setCreative(initialCreative);
                navigate(-1);
              }}
            >
              Annuler
            </Button>
          </Grid>
          <Grid item>
            <Button
              disabled={props.mode === "create" || !creative.id}
              onClick={() => deleteCreativeMutation.mutate(creative.id)}
              variant="outlined"
            >
              Supprimer
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default CreativeEditController;
