import { QueryKey, useQuery } from "@tanstack/react-query";
import { Creative } from "../utils/interfaces";
import CreativeList from "../components/CreativeList";
import React from "react";
import { Grid, Pagination } from "@mui/material";
import CreativeDynamicPreview from "../components/CreativeDynamicPreview";
import { useNavigate, useSearchParams } from "react-router-dom";

interface CreativeListControllerProps {
  nbOfItemsPerPage?: number;
}

const DEFAULT_NB_OF_ITEMS_PER_PAGE = 5;

function CreativeListController(props: CreativeListControllerProps) {
  let navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const [page, setPage] = React.useState<number>(
    Number(searchParams.get("page") ?? 1)
  );
  const [lastPage, setLastPage] = React.useState<number>(
    Number(searchParams.get("page") ?? 1)
  );
  const [selectedId, setSelectedId] = React.useState<string | undefined>(
    searchParams.get("selectedId") ?? undefined
  );

  React.useEffect(() => {
    const newPage = Number(searchParams.get("page") ?? 1);
    // On vérifie que la page demandée existe bien
    if (newPage > lastPage) {
      setPage(lastPage);
      navigate(`/?page=${lastPage}&selectedId=${selectedId}`);
    } else if (newPage <= 0) {
      setPage(1);
      navigate(`/?page=1&selectedId=${selectedId}`);
    } else {
      setPage(newPage);
    }
  }, [searchParams]);

  const fetchCreatives = ({
    queryKey: [_queryName, queryPage],
  }: {
    queryKey: QueryKey;
  }) =>
    fetch(
      `http://localhost:3001/creatives?_page=${queryPage ?? 1}&_limit=${
        props.nbOfItemsPerPage ?? DEFAULT_NB_OF_ITEMS_PER_PAGE
      }`
    ).then(async (res) => {
      const result = await res.json();
      const localTotalCount = Number(res.headers.get("X-Total-Count") ?? "0");
      const localLastPage = Math.ceil(
        localTotalCount /
          (props.nbOfItemsPerPage ?? DEFAULT_NB_OF_ITEMS_PER_PAGE)
      );
      // Si la page où on se trouvait avant d'éditer et/ou supprimer une bannière
      // n'existe plus après avoir réalisé l'action alors on redirige vers la dernière page existante
      if (result.length === 0 && localTotalCount > 0) {
        setPage(localLastPage);
      }
      setLastPage(localLastPage);
      return result;
    });

  const { isLoading, isFetching, error, data } = useQuery<Creative[]>(
    ["listCreatives", page],
    fetchCreatives,
    {
      keepPreviousData: true,
    }
  );

  if (isLoading) return <div>Chargement...</div>;

  if (error)
    return <div>Une erreur est survenue : {(error as any).message}</div>;

  return (
    <>
      <Grid container spacing={2} justifyContent="center">
        <Grid item xs={12}>
          <CreativeList
            creatives={data ?? []}
            selectedId={selectedId}
            onClickItem={(id) => {
              setSelectedId(id);
              navigate(`/?page=${page}&selectedId=${id}`);
            }}
          />
        </Grid>
        <Grid item>
          <Pagination
            count={lastPage}
            page={page}
            onChange={(_e, page: number) => {
              navigate(`/?page=${page}&selectedId=${selectedId}`);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <CreativeDynamicPreview
            creative={
              selectedId ? data?.find(({ id }) => selectedId === id) : undefined
            }
            handleClickEdit={() => navigate(`/edit/${selectedId}`)}
          />
        </Grid>
      </Grid>
      {isFetching ? <span>Chargement...</span> : null}
    </>
  );
}

export default CreativeListController;
