export interface User {
  id: string;
  firstName: string;
  lastName: string;
}

export interface Format {
  width: number;
  height: number;
}

export interface Creative {
  id: string;
  enabled: boolean;
  content: string;
  description: string;
  createdBy: User;
  lastModified: Date;
  title: string;
  formats: Format[];
  contributors: User[];
}
