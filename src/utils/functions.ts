import { Format, User } from "./interfaces";

export function getUserInitials(user: User): string {
  return (user.firstName[0] + user.lastName[0]).toUpperCase();
}

export function formatToString(format: Format): string {
  return `${format.width}x${format.height}`;
}
