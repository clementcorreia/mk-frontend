import { formatToString, getUserInitials } from "./functions";
import { Format, User } from "./interfaces";

test("getUserInitials function must return the right initials", () => {
  const user: User = { id: "uuid", firstName: "Clément", lastName: "Correia" };
  const initialsTest = getUserInitials(user);
  expect(initialsTest).toEqual("CC");
});

test("formatToString function must return the right string", () => {
  const format1: Format = { width: 200, height: 300 };
  const format2: Format = { height: 200, width: 300 };
  const result1 = formatToString(format1);
  const result2 = formatToString(format2);
  expect(result1).toEqual("200x300");
  expect(result2).toEqual("300x200");
});
