import { Creative } from "./interfaces";

export const testCreative: Creative = {
  id: "myCreativeId",
  title: "Titre 1",
  content: "My creative content",
  description: "My creative description",
  enabled: true,
  formats: [
    { width: 300, height: 250 },
    { width: 728, height: 90 },
    { width: 160, height: 600 },
  ],
  createdBy: {
    id: "myUserId",
    firstName: "Clément",
    lastName: "Correia",
  },
  lastModified: new Date("2022-01-01"),
  contributors: [
    { id: "myUserId", firstName: "Clément", lastName: "Correia" },
    { id: "myUserId2", firstName: "Jane", lastName: "Doe" },
  ],
};
