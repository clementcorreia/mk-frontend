import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { Grid } from "@mui/material";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import CreativeListController from "./controllers/CreativeListController";
import CreativeEditController from "./controllers/CreativeEditController";

const queryClient = new QueryClient();

function App() {
  return (
    <Router>
      <QueryClientProvider client={queryClient}>
        <Grid container style={{ marginTop: 16, marginBottom: 16 }} spacing={3}>
          <Grid item xs={2} />
          <Grid item xs={8}>
            <Routes>
              <Route
                path="/edit/new"
                element={<CreativeEditController mode="create" />}
              ></Route>
              <Route
                path="/edit/:creativeId"
                element={<CreativeEditController mode="edit" />}
              ></Route>
              <Route path="/" element={<CreativeListController />}></Route>
            </Routes>
          </Grid>
        </Grid>
      </QueryClientProvider>
    </Router>
  );
}

export default App;
